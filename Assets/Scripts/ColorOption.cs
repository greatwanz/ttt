﻿using UnityEngine;
using UnityEngine.UI;

namespace TTT
{
    [RequireComponent(typeof(Image))]
    public class ColorOption : MonoBehaviour
    {
        [AssertNotNull] [SerializeField] private Outline outline;
        [AssertNotNull] [SerializeField] private IntIntGameEvent changeColorEvent;
        private Image image;

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        public void SetImageColor(Color color)
        {
            image.color = color;
        }

        public void SetFocusState(bool focus)
        {
            outline.enabled = focus;
        }

        public void ChangeColor()
        {

            changeColorEvent.Raise(transform.GetSiblingIndex(), GetComponentInParent<ColorOptionsManager>().playerdef.playerID);
        }
    }
}