﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace TTT
{
    [Serializable]
    public enum SELECTION_STATE
    {
        NONE = 0,
        PLAYER_1 = 1,
        PLAYER_2 = 2,
    }

    [RequireComponent(typeof(RectTransform))]
    public class GameManager : MonoBehaviour
    {
        [Header("Reference")]
        [AssertNotNull] [SerializeField] private GameObject postGameOptions;
        [Space]
        [AssertNotNull] [SerializeField] private Text resultText;
        [AssertNotNull] [SerializeField] private Text activePlayerText;
        [Header("Data")]
        [AssertNotNull] [SerializeField] private IntVariable rowCount;
        [AssertNotNull] [SerializeField] private IntVariable totalMoves;
        [Space]
        [AssertNotNull] [SerializeField] private StringVariable drawText;
        [AssertNotNull] [SerializeField] private StringVariable winText;
        [AssertNotNull] [SerializeField] private StringVariable playerText;
        [Space]
        [AssertNotNull] [SerializeField] private ActivePlayerVariable activePlayer;

        private SELECTION_STATE[,] quadStateArray;

        private void Awake()
        {
            quadStateArray = new SELECTION_STATE[rowCount.value, rowCount.value];

            for (int i = 0; i < rowCount.value; i++)
            {
                for (int j = 0; j < rowCount.value; j++)
                {
                    quadStateArray[i, j] = SELECTION_STATE.NONE;
                }
            }

            totalMoves.Set(0);
            activePlayer.Set(SELECTION_STATE.PLAYER_1);
            activePlayerText.text = string.Format(playerText.value, (int)activePlayer.value);
        }

        public void RestartGame()
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        public void ReturnToMenu()
        {
            SceneManager.LoadScene("MainMenuScene");
        }

        public void CheckState(Vector2Int placedQuadrant)
        {
            if (activePlayer.value == SELECTION_STATE.NONE && quadStateArray[placedQuadrant.x, placedQuadrant.y] != SELECTION_STATE.NONE)
                return;

            quadStateArray[placedQuadrant.x, placedQuadrant.y] = activePlayer.value;

            if (CheckDraw())
            {
                resultText.text = drawText.value;
                EndGameState();
            }
            else if (CheckWin(placedQuadrant))
            {
                resultText.text = string.Format(winText.value, (int)activePlayer.value);
                EndGameState();
            }
            else
            {
                activePlayer.Set(totalMoves.value % 2 == 0 ? SELECTION_STATE.PLAYER_1 : SELECTION_STATE.PLAYER_2);
                activePlayerText.text = string.Format(playerText.value, (int)activePlayer.value);
            }
        }

        private void EndGameState()
        {
            activePlayer.Set(SELECTION_STATE.NONE);
            postGameOptions.SetActive(true);
            activePlayerText.gameObject.SetActive(false);
        }

        private bool CheckWin(Vector2Int placedQuadrant)
        {
            return CheckColumnWinCondition(placedQuadrant) || CheckRowWinCondition(placedQuadrant) || CheckDiagonalWinCondition(placedQuadrant);
        }

        private bool CheckDraw()
        {
            return totalMoves.value == quadStateArray.Length;
        }

        private bool CheckRowWinCondition(Vector2Int placedQuadrant)
        {
            for (int i = 0; i < rowCount.value; i++)
            {
                if (quadStateArray[placedQuadrant.x, i] != activePlayer.value)
                {
                    return false;
                }
            }

            return true;
        }

        private bool CheckColumnWinCondition(Vector2Int placedQuadrant)
        {
            for (int i = 0; i < rowCount.value; i++)
            {
                if (quadStateArray[i, placedQuadrant.y] != activePlayer.value)
                {
                    return false;
                }
            }

            return true;
        }

        private bool CheckDiagonalWinCondition(Vector2Int placedQuadrant)
        {
            //Check top left to bottom right diagonal
            if (placedQuadrant.x == placedQuadrant.y)
            {
                for (int i = 0; i < rowCount.value; i++)
                {
                    if (quadStateArray[i, i] != activePlayer.value)
                    {
                        return false;
                    }
                }

                return true;
            }
            //Check top right to bottom left diagonal
            else if (placedQuadrant.x + placedQuadrant.y == rowCount.value - 1)
            {
                for (int i = 0; i < rowCount.value; i++)
                {
                    if (quadStateArray[i, rowCount.value - 1 - i] != activePlayer.value)
                    {
                        return false;
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}