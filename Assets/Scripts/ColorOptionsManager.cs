﻿using UnityEngine;
using UnityEngine.UI;

namespace TTT
{
    public class ColorOptionsManager : MonoBehaviour
    {
        [Header("Reference")]
        [AssertNotNull] [SerializeField] private GridLayoutGroup gridLayoutGroup;
        [AssertNotNull] [SerializeField] private Text playerTextField;
        [AssertNotNull] [SerializeField] private Image image;
        [Header("Data")]
        [AssertNotNull] [SerializeField] private ColoursArray coloursArray;
        [AssertNotNull] [SerializeField] private PlayerDefinition playerDefinition;
        [AssertNotNull] [SerializeField] private StringVariable playerText;
        [Header("Prefab")]
        [AssertNotNull] [SerializeField] private ColorOption colorOptionPrefab;

        public PlayerDefinition playerdef { get { return playerDefinition; } }

        ColorOption[] options;
        ColorOptionsManager[] com;

        private void Awake()
        {
            playerDefinition.SetCurrentColorIndex(playerDefinition.playerID);
            image.sprite = playerDefinition.sprite;
            playerTextField.text = string.Format(playerText.value, playerDefinition.playerID + 1);

            for (int i = 0; i < coloursArray.value.Length; i++)
            {
                ColorOption co = Instantiate(colorOptionPrefab, gridLayoutGroup.transform);
                co.SetImageColor(coloursArray.value[i]);
                if (i == playerDefinition.currentColorIndex)
                {
                    co.SetFocusState(true);
                }
            }

            options = GetComponentsInChildren<ColorOption>();
        }

        public void ColorCheck(int index, int id)
        {
            if (playerDefinition.playerID != id && playerDefinition.currentColorIndex == index)
            {
                return;
            }

            SetColor(index, id);
        }

        public void SetColor(int index, int id)
        {
            if (playerDefinition.playerID == id)
            {
                playerTextField.color = coloursArray.value[index];
                image.color = coloursArray.value[index];
                options[playerDefinition.currentColorIndex].SetFocusState(false);
                options[index].SetFocusState(true);
                playerDefinition.SetCurrentColorIndex(index);
            }
        }
    }
}