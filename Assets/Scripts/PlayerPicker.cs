﻿using UnityEngine;

namespace TTT
{
    public class PlayerPicker : MonoBehaviour
    {
        [Header("Prefabs")]
        [AssertNotNull] [SerializeField] private ColorOptionsManager[] com;
        [Header("Data")]
        [AssertNotNull] [SerializeField] private ColoursArray coloursArray;
        private void Awake()
        {
            for (int i = 0; i < com.Length; i++)
            {
                ColorOptionsManager player = Instantiate(com[i], transform);
                player.SetColor(i, player.playerdef.playerID);
            }
        }
    }
}