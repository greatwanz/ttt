﻿using UnityEngine;
using UnityEngine.UI;

namespace TTT
{
    [RequireComponent(typeof(Button))]
    public class Quadrant : MonoBehaviour
    {
        [Header("References")]
        [AssertNotNull] [SerializeField] private PlayerDefinition[] definitions;
        [Header("Data")]
        [AssertNotNull] [SerializeField] private ActivePlayerVariable activePlayer;
        [Space]
        [AssertNotNull] [SerializeField] private ColoursArray coloursArray;
        [Space]
        [AssertNotNull] [SerializeField] private IntVariable totalMoves;
        [AssertNotNull] [SerializeField] private IntVariable rowCount;
        [Header("Game Events")]
        [AssertNotNull] [SerializeField] private Vector2IntGameEvent selectQuadrantEvent;

        private Button button;
        Vector2Int quadrantIndex;

        private void Awake()
        {
            button = GetComponent<Button>();
        }

        private void Start()
        {
            int childIndex = transform.GetSiblingIndex();
            quadrantIndex = new Vector2Int(childIndex / rowCount.value, childIndex % rowCount.value);
        }

        private void OnQuadrantClicked()
        {
            if (button.image.sprite == null)
            {
                if (activePlayer.value != SELECTION_STATE.NONE)
                {
                    PlayerDefinition def = definitions[((int)activePlayer.value) - 1];
                    button.image.sprite = def.sprite;
                    button.image.color = coloursArray.value[def.currentColorIndex];
                }
                else
                {
                    return;
                }

                totalMoves++;
                selectQuadrantEvent.Raise(quadrantIndex);
            }
        }
    }
}