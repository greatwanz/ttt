﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTT
{
    [CreateAssetMenu(menuName = "Variables/String")]
    public class StringVariable : Variable<string>
    {

    }
}