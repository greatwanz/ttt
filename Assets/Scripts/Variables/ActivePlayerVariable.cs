﻿using UnityEngine;

namespace TTT
{
    [CreateAssetMenu(menuName = "Variables/Active Player")]
    public class ActivePlayerVariable : Variable<SELECTION_STATE>
    {

    }
}