﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTT
{
    public abstract class Variable<T> : ScriptableObject
    {
        [SerializeField] private T _value;

        public T value { get { return _value; } }

        public void Set(T val)
        {
            _value = val;
        }
    }
}