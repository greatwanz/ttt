﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTT
{
    [CreateAssetMenu(menuName = "Variables/Integers")]
    public class IntVariable : Variable<int>
    {
        public static IntVariable operator ++(IntVariable a)
        {
            a.Set(a.value + 1);
            return a;
        }
    }
}