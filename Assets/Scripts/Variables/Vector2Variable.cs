﻿using UnityEngine;

namespace TTT
{
    [CreateAssetMenu(menuName = "Variables/Vector2")]
    public class Vector2Variable : Variable<Vector2>
    {

    }
}