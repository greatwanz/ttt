﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace TTT
{
    public class TitleScript : MonoBehaviour
    {
        [Header("References")]
        [AssertNotNull] [SerializeField] private Text titleText;
        [Header("Data")]
        [AssertNotNull] [SerializeField] private StringVariable title;

        private void Awake()
        {
            titleText.text = title.value;
        }

        public void StartGame()
        {
            SceneManager.LoadScene("GameScene");
        }
    }
}