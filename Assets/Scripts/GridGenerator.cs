﻿using UnityEngine;
using UnityEngine.UI;

namespace TTT
{
    [RequireComponent(typeof(GridLayoutGroup), typeof(RectTransform))]
    public sealed class GridGenerator : MonoBehaviour
    {
        [Header("References")]
        [AssertNotNull] [SerializeField] private Button quadrant;
        [Header("Data")]
        [AssertNotNull] [SerializeField] private IntVariable cellSize;
        [AssertNotNull] [SerializeField] private IntVariable rowCount;

        private GridLayoutGroup gridLayoutGroup;
        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            gridLayoutGroup = GetComponent<GridLayoutGroup>();

            gridLayoutGroup.cellSize = new Vector2(cellSize.value, cellSize.value);
            gridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedRowCount;
            gridLayoutGroup.constraintCount = rowCount.value;
            rectTransform.sizeDelta = gridLayoutGroup.cellSize * rowCount.value;

            int gridSize = rowCount.value * rowCount.value;
            for (int i = 0; i < gridSize; i++)
            {
                Instantiate(quadrant, transform);
            }
        }
        // Use this for initialization
        void Start()
        {
        }
    }
}