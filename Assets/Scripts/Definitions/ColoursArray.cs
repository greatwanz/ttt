﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TTT
{
    [CreateAssetMenu(menuName = "Arrays/Colours")]
    public class ColoursArray : ScriptableObject
    {
        [AssertNotNull] [SerializeField] private Color[] colours;

        public Color[] value
        {
            get { return colours; }
        }
    }
}