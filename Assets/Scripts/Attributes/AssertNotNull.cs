﻿using System;

namespace TTT
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AssertNotNull : Attribute
    {
    }
}