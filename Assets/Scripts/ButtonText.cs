﻿using UnityEngine;
using UnityEngine.UI;

namespace TTT
{
    [RequireComponent(typeof(Text))]
    public class ButtonText : MonoBehaviour
    {
        [AssertNotNull] [SerializeField] private StringVariable textToDisplay;

        private Text text;

        void Awake()
        {
            text = GetComponent<Text>();
            text.text = textToDisplay.value;
        }
    }
}