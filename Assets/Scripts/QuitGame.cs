﻿using UnityEngine;

namespace TTT
{
    public class QuitGame : MonoBehaviour
    {
        public void Quit()
        {
            Application.Quit();
        }
    }
}