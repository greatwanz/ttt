﻿using UnityEngine;
using UnityEngine.Events;

namespace TTT
{
    [System.Serializable]
    public class Vector2IntUnityEvent : UnityEvent<Vector2Int>
    {
    }


    public class Vector2IntGameEventListener : MonoBehaviour
    {
        [SerializeField] protected Vector2IntGameEvent gameEvent;
        [SerializeField] protected Vector2IntUnityEvent response;

        public virtual void OnEnableLogic()
        {
            if (gameEvent != null)
                gameEvent.Register(this);
        }

        void OnEnable()
        {
            OnEnableLogic();
        }

        public virtual void OnDisableLogic()
        {
            if (gameEvent != null)
                gameEvent.UnRegister(this);
        }

        void OnDisable()
        {
            OnDisableLogic();
        }

        public virtual void Response(Vector2Int val)
        {
            response.Invoke(val);
        }
    }
}
