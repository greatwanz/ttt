﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTT
{
    [CreateAssetMenu(menuName = "GameEvent/Int Game Event")]
    public class IntIntGameEvent : ScriptableObject
    {
        List<IntIntGameEventListener> listeners = new List<IntIntGameEventListener>();

        public void Register(IntIntGameEventListener l)
        {
            listeners.Add(l);
        }

        public void UnRegister(IntIntGameEventListener l)
        {
            listeners.Remove(l);
        }

        public void Raise(int val, int val2)
        {
            for (int i = 0; i < listeners.Count; i++)
            {
                listeners[i].Response(val, val2);
            }
        }
    }
}
