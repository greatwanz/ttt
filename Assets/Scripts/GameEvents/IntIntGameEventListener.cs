﻿using UnityEngine;
using UnityEngine.Events;

namespace TTT
{
    [System.Serializable]
    public class IntIntUnityEvent : UnityEvent<int, int>
    {
    }


    public class IntIntGameEventListener : MonoBehaviour
    {
        [SerializeField] protected IntIntGameEvent gameEvent;
        [SerializeField] protected IntIntUnityEvent response;

        public virtual void OnEnableLogic()
        {
            if (gameEvent != null)
                gameEvent.Register(this);
        }

        void OnEnable()
        {
            OnEnableLogic();
        }

        public virtual void OnDisableLogic()
        {
            if (gameEvent != null)
                gameEvent.UnRegister(this);
        }

        void OnDisable()
        {
            OnDisableLogic();
        }

        public virtual void Response(int val, int val2)
        {
            response.Invoke(val, val2);
        }
    }
}
