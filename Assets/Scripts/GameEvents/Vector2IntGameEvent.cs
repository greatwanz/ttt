﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTT
{
    [CreateAssetMenu(menuName = "GameEvent/Vector2Int Game Event")]
    public class Vector2IntGameEvent : ScriptableObject
    {
        List<Vector2IntGameEventListener> listeners = new List<Vector2IntGameEventListener>();

        public void Register(Vector2IntGameEventListener l)
        {
            listeners.Add(l);
        }

        public void UnRegister(Vector2IntGameEventListener l)
        {
            listeners.Remove(l);
        }

        public void Raise(Vector2Int val)
        {
            for (int i = 0; i < listeners.Count; i++)
            {
                listeners[i].Response(val);
            }
        }
    }
}
