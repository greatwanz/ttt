﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTT
{
    [CreateAssetMenu(menuName = "Definition/Player")]
    public class PlayerDefinition : ScriptableObject
    {
        [Header("Definition")]
        [AssertNotNull] [SerializeField] private Sprite _sprite;
        [SerializeField] private int _playerID;
        [SerializeField] private int _currentColorIndex;

        public Sprite sprite { get { return _sprite; } }

        public int playerID { get { return _playerID; } }
        public int currentColorIndex { get { return _currentColorIndex; } }

        public void SetCurrentColorIndex(int index)
        {
            _currentColorIndex = index;
        }
    }
}