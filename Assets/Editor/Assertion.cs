﻿using System;
using System.IO;
using System.Reflection;
using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TTT
{
    public class Assertion
    {
        static string ns;
        [MenuItem("Tools/Assert Fields")]
        static void EditorAssert()
        {
            var logEntries = Type.GetType("UnityEditor.LogEntries,UnityEditor.dll");
            var clearMethod = logEntries.GetMethod("Clear", BindingFlags.Static | BindingFlags.Public);
            clearMethod.Invoke(null, null);
            AttributeAssert();
            Debug.Log("Assertion compete");
        }

        public static void AttributeAssert()
        {
            string[] splitNS = typeof(Assertion).Namespace.Split('.');
            ns = splitNS[0];

            //Assert scripts in hierarchy
            var root = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();

            foreach (var r in root)
            {
                MonoBehaviour[] sceneActive = r.GetComponentsInChildren<MonoBehaviour>(true);
                foreach (MonoBehaviour script in sceneActive)
                {
                    var scriptType = script.GetType();
                    while (scriptType.Namespace != null && scriptType.Namespace.Contains(ns))
                    {
                        var fields = scriptType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                        AssertFieldNotNull(script, scriptType, fields);
                        scriptType = scriptType.BaseType;
                    }
                }
            }

#if UNITY_EDITOR
            //Assert SOs
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(ScriptableObject).Name);
            for (int i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                var so = AssetDatabase.LoadAssetAtPath<ScriptableObject>(path);
                var scriptType = so.GetType();

                while (scriptType.Namespace != null && scriptType.Namespace.Contains(ns))
                {
                    var fields = scriptType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    AssertFieldNotNull(so, scriptType, fields);
                    scriptType = scriptType.BaseType;
                }
            }

            //Assert all prefabs
            string[] prefabs = AssetDatabase.FindAssets("t:prefab");
            for (int i = 0; i < prefabs.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(prefabs[i]);
                var so = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                var mb = so.GetComponentsInChildren<MonoBehaviour>(true);
                foreach (MonoBehaviour script in mb)
                {
                    if (script == null)
                        continue;

                    var scriptType = script.GetType();

                    while (scriptType.Namespace != null && scriptType.Namespace.Contains(ns))
                    {
                        var fields = scriptType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                        AssertFieldNotNull(Path.GetFileNameWithoutExtension(path), script, scriptType, fields);
                        scriptType = scriptType.BaseType;
                    }
                }
            }
#endif
        }

        /// <summary>
        /// SO assert
        /// </summary>
        /// <param name="script">script containing the unassigned field</param>
        /// <param name="scriptType">Type of the script</param>
        /// <param name="fields">FieldInfo of script</param>
        private static void AssertFieldNotNull(UnityEngine.Object script, Type scriptType, FieldInfo[] fields)
        {
            foreach (var field in fields)
            {
                var assertNotNulls = field.GetCustomAttributes(typeof(AssertNotNull), true);
                if (assertNotNulls.Length < 1)
                    continue;

                var value = field.GetValue(script);
                if (value == null || (value is UnityEngine.Object && (UnityEngine.Object)value == null))
                {
                    Debug.LogErrorFormat(script, "ScriptableObject '{0}', Script '{1}', field '{2}' is unassigned.", script.name, scriptType.Name, field.Name);
                }
                else
                {
                    AssertStruct(script, scriptType, value);
                    AssertICollection(script, scriptType, value);
                }
            }
        }

        private static void AssertFieldNotNull(string g, MonoBehaviour script, Type scriptType, FieldInfo[] fields)
        {
            foreach (var field in fields)
            {
                var assertNotNulls = field.GetCustomAttributes(typeof(AssertNotNull), true);
                if (assertNotNulls.Length < 1)
                    continue;

                var value = field.GetValue(script);
                if (value == null || (value is UnityEngine.Object && (UnityEngine.Object)value == null))
                {
                    Debug.LogErrorFormat(script, "Prefab {0} with script '{1}', field '{2}' is unassigned.", g, script.name, field.Name);
                }
                else
                {
                    AssertStruct(script, scriptType, value);
                    AssertICollection(script, scriptType, value);
                }
            }
        }

        private static void AssertICollection(UnityEngine.Object script, Type scriptType, object col)
        {
            if (col is ICollection)
            {
                ICollection collection = (ICollection)col;

                int i = 0;
                foreach (object o in collection)
                {
                    if (o == null || (o is UnityEngine.Object && (UnityEngine.Object)o == null))
                    {
                        Debug.LogErrorFormat(script, "Script '{0}', collection '{1}', index {2} contains an empty element.",
                            scriptType.Name, col.ToString(), i);
                    }
                    else
                    {
                        if (o.GetType().Namespace.Contains(ns))
                        {
                            AssertICollection(script, scriptType, o);
                            AssertStruct(script, scriptType, o);
                        }
                    }
                    i++;
                }
            }
        }

        private static void AssertStruct(UnityEngine.Object script, Type scriptType, object o)
        {
            if (o.GetType().IsValueType && o.GetType().Namespace.Contains(ns))
            {
                foreach (var f in o.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
                {
                    var v = f.GetValue(o);
                    if (v == null || (v is UnityEngine.Object && (UnityEngine.Object)v == null))
                    {
                        Debug.LogErrorFormat(script, "Script '{0}', field '{1}' in struct {2} is unassigned.", scriptType.Name, f.Name, o.ToString());
                    }
                    else
                    {
                        AssertICollection(script, scriptType, v);
                        AssertStruct(script, scriptType, v);
                    }
                }
            }
        }
    }
}